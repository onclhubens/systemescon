
import java.net.MalformedURLException;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Hashtable;

public class Server extends UnicastRemoteObject implements Server_itf {

	private Hashtable<Integer, ServerObject> Stockage = new Hashtable<Integer, ServerObject>();
	private Hashtable<String, ServerObject> ServeurNom = new Hashtable<String, ServerObject>();
	private int IDforNaming;
	public static int port = 9999;
	public String name = "tiensbonjourslutdismoicommenttuvas";
	
	protected Server() throws RemoteException {
		super();
		System.out.println("Serveur cree");
		this.IDforNaming=0;
		
		try {
			Naming.rebind("rmi://127.0.0.1:"+port+"/Serveur", this );
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Serveur ok");
		
	}
		
	@Override
	public synchronized int lookup(String name) throws RemoteException {
		try{
			
			System.out.println("Serveur : recherche de " + name);
	

			if(ServeurNom.get(name)==null){
				System.out.println("Serveur : " + name+ " non trouvé");

				return -1;
			}else{
				System.out.println("Serveur : " + name+ " trouvé");

				return ServeurNom.get(name).getID();
			}
		}catch(Exception e){
			System.out.println("Serveur : " + name+ " non trouvé : erreur");

			return -1;
		}
		
	}

	@Override
	public synchronized void register(String name, int id) throws RemoteException {
		ServeurNom.put(name, Stockage.get(id));

	}

	@Override
	public synchronized int create(Object o) throws RemoteException {
		IDforNaming+=1;	
		ServerObject temp = new ServerObject(o, IDforNaming,null);
		Stockage.put(IDforNaming, temp);
		return IDforNaming;
	}

	@Override
	public synchronized Object lock_read(int id, Client_itf client) throws RemoteException {

		
		System.out.println("Serveur : lock read "+ id );		
		Stockage.get(id).lock_read(client);	
		System.out.println("Serveur : lock read "+ id +" ok");

		
		return Stockage.get(id).obj;
	}

	@Override
	public synchronized Object lock_write(int id, Client_itf client) throws RemoteException {

		System.out.println("Serveur : lock write "+ id );

		
		Stockage.get(id).lock_write(client);
		
		System.out.println("Serveur : lock write "+ id +" ok");

		return Stockage.get(id).obj;
	}
	
	public static void main(String[] args){
		try {
			LocateRegistry.createRegistry(Server.port);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			Server server = new Server();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}