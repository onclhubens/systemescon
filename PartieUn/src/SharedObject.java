import java.io.*;
import java.rmi.server.UnicastRemoteObject;

public class SharedObject implements Serializable, SharedObject_itf {

	private TypeOfLock lock;
	public Object obj;
	private int ID;
	private Client_itf client;



	public SharedObject(Object obj, int iD, Client_itf clienttemp) {
		super();
		this.lock=TypeOfLock.RLT;
		this.obj = obj;
		this.client=clienttemp;
		this.ID = iD;

	}

	public int getID() {
		return ID;
	}

	// invoked by the user program on the client node
	/* Bloquer l'objet en lecture, çad empêcher les écritures. */
	public   void lock_read() {

		switch(this.lock){
		case RLC:
			//System.out.println("Shared Object : RLC lock read modif obj "+ ID);

			this.obj=Client.lock_read(this.ID);
			this.lock=TypeOfLock.RLT;
			break;
		case NL:
			//System.out.println("Shared Object : NL lock read modif obj "+ ID);

			this.obj=Client.lock_read(this.ID);
			this.lock=TypeOfLock.RLT;
			break;
		case WLC:
			//System.out.println("Shared Object : WLC lock read modif obj "+ ID);

			this.obj=Client.lock_read(this.ID);
			this.lock=TypeOfLock.RLT_WLC;
			break;
		default:
			//System.out.println("Shared Object : lock read ne fait rien");

			break;
		}
	}

	// invoked by the user program on the client node
	public   void lock_write() {


		switch(this.lock){
		case RLT:
			//System.out.println("Shared Object : RLT lock write modif obj "+ ID);
			
			this.obj=Client.lock_write(this.ID);
			this.lock=TypeOfLock.WLT;
			break;
		case RLC:
			//System.out.println("Shared Object : RLC lock write modif obj "+ ID);

			this.obj=Client.lock_write(this.ID);
			this.lock=TypeOfLock.WLT;
			break;
		case NL:
			//System.out.println("Shared Object : NL lock write modif obj "+ ID);

			this.obj=Client.lock_write(this.ID);			
			this.lock=TypeOfLock.WLT;
			break;
		case WLC:
			//System.out.println("Shared Object : WLC lock write modif obj "+ ID);

			this.obj=Client.lock_write(this.ID);
			this.lock=TypeOfLock.WLT;
			break;
		default:
			//System.out.println("Shared Object : lock write ne fait rien");

			break;
		}
	}

	// invoked by the user program on the client node
	public synchronized void unlock() {
		//System.out.println("Shared Object : unlock "+this.ID);

		
		switch(this.lock){
		case RLT:
			this.lock=TypeOfLock.RLC;
			break;
		case RLT_WLC:
			this.lock=TypeOfLock.WLC;
			break;
		case WLT:
			this.lock=TypeOfLock.WLC;
			break;
		default:
		//	System.out.println("Shared Object : unlock ne fait rien");

			break;

		}
		notify();
	}


	// callback invoked remotely by the server
	public synchronized  Object reduce_lock() {

	//	System.out.println("Shared Object : reduce lock de "+this.ID);
		
		  
		
		
		switch(this.lock){
		case WLT:
			try {
				wait();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			this.lock=TypeOfLock.RLC;
			break;
		case RLT_WLC:
			try {
				wait();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			this.lock=TypeOfLock.RLC;
			break;
		default:
		//	System.out.println("Shared Object : reduce lock ne fait rien");

			break;
		}
		//System.out.println("Shared Object : reduce lock notification");

		return this.obj;
	}

	// callback invoked remotely by the server
	public synchronized void invalidate_reader() {
		
    	//System.out.println("Shared Object : attente d'invalidation en lecture");

		
		
		switch(this.lock){
		case RLT:
			try {
				wait();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			this.lock=TypeOfLock.NL;
			break;
		case RLC:
		//    	System.out.println("Shared Object : attente d'invalidation en lecture dans RLC");
		
			this.lock=TypeOfLock.NL;
			break;
		case WLC:
			
		 //   	System.out.println("Shared Object : attente d'invalidation en lecture dans WLC");
		    	try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			this.lock=TypeOfLock.NL;
			break;
		default:
		//	System.out.println("Shared Object : invalidate reader ne fait rien");

			break;
		}
		
		//System.out.println("Shared Object : ecriture invalidee");

	}

	public synchronized Object invalidate_writer() {

		//System.out.println("Shared Object : attente d'invalidation en ecriture");

		
		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.lock=TypeOfLock.NL;
		//System.out.println("Shared Object : ecriture invalidee");

		return obj;
	}
}
