import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.*;
import java.util.*;
import java.net.*;

public class Client extends UnicastRemoteObject implements Client_itf {

	// Les objets utilisés par le client.
	private static Hashtable<Integer, String> Nommage = new Hashtable<Integer, String>();
	private static Hashtable<Integer, SharedObject> Stockage = new Hashtable<Integer,SharedObject>();
	private static Hashtable<SharedObject,Integer> StockageBis = new Hashtable<SharedObject,Integer>();



	/* Le serveur qui contient les objets.
	   Il est commun à tous les clients, c'est donc une variable statique/variable de classe. */
	static Server_itf monServeur;
	private static Client client;
	
	public Client() throws RemoteException {
		super();
	}


	///////////////////////////////////////////////////
	//         Interface to be used by applications
	///////////////////////////////////////////////////

	// initialization of the client layer
	public static void init() {
		/* On cherche, dans l'annuaire RMI, le truc qui s'appelle "Serveur". */
		
		try {
			
		//	System.out.println("rmi://127.0.0.1:"+Server.port+"/Serveur");

			Client.monServeur=(Server_itf)Naming.lookup("rmi://127.0.0.1:"+Server.port+"/Serveur");
			

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			Client.client= new Client();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//System.out.println(" init() ok");
	}

	// lookup in the name server
	public static SharedObject lookup(String name) {

		int id;
		//System.out.println("Client : recherche de " + name);

		try {
			
			id = Client.monServeur.lookup(name);
			
			if (id==-1){
				//System.out.println("Client : pas trouv� " + name);

				return null;
			}
			
			//System.out.println("Client : " +name+" trouvé -> lockread");

			Object zeObjet = Client.monServeur.lock_read(id, (Client_itf)Client.client);

			SharedObject so=new SharedObject(zeObjet, id,(Client_itf)client);
			
			so.unlock();
			
			//System.out.println("Client.Nommage.size()="+Client.Nommage.size());
			Client.Nommage.put(id, name);
			//System.out.println("Client.client.Stockage.size()="+Client.client.Stockage.size());
			Client.client.Stockage.put(id, so);
		//	System.out.println("Client.client.StockageBis.size()="+Client.client.StockageBis.size());
			Client.client.StockageBis.put(so,id);

			
			
			//System.out.println("Client : " +name+" tentative stockage ");
			//System.out.println("Client : " +name+" stocké ");

			return so;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
			
	}
	
	
	// binding in the name server
	public static void register(String name, SharedObject_itf so) {
		
		//System.out.println("Client : register " + name);
		
		if (so == null) {
			System.out.println("Client : so null");

		}
		//System.out.println("Client : ID =  " + Client.StockageBis.get(so));
		try {
			monServeur.register(name, Client.StockageBis.get(so));
		} catch (RemoteException e) {
			//System.out.println("Client : register oups");

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println("Client : "+ name + " registered");

		
		
	}

	// creation of a shared object
	public static SharedObject create(Object o) {
		
		//System.out.println("Client : creation d'un objet");

			
		int id = 0;
		SharedObject so = null;
		try {
			id =  monServeur.create(o);
			so = new SharedObject(o,id,(Client_itf)client);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		if (so==null){
			//System.out.println("Client : attention, creation nulle ");

		}
		System.out.println("Client : create : ID = " +id);

		Client.Stockage.put(id, so);
		Client.StockageBis.put(so, id);
		//System.out.println("Client : objet cree");
		so.unlock();
		return so;
		
	}
	
	/////////////////////////////////////////////////////////////
	//    Interface to be used by the consistency protocol
	////////////////////////////////////////////////////////////

	// demande d'un verrou au serveur par le client
	public static Object lock_read(int id) {
		try {

			
			//System.out.println("Client  : demande de lecture "+ id);

			Object temp = monServeur.lock_read(id, (Client_itf)client);
			//System.out.println("Client  : lecture OK");

			return temp;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}

	// demande d'un verrou au serveur par le client
	public static Object lock_write (int id) {
		try {
		//	System.out.println("Client  : demande d'ecriture "+ id);

			Object temp = monServeur.lock_write(id, (Client_itf)client);
			
		//	System.out.println("Client  : ecriture OK");

			return temp;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	// receive a lock reduction request from the server
	public synchronized Object reduce_lock(int id) throws java.rmi.RemoteException {
		//System.out.println("Client  : reduce_lock de "+id);
	
		//System.out.println("client.Stockage.size()="+client.Stockage.size());
	
		SharedObject so = client.Stockage.get(id);
		if (so==null){
			//System.out.println("Client  : reduce_lock so null");

		}
		return so.reduce_lock();

		
	}


	// receive a reader invalidation request from the server
	public synchronized void invalidate_reader(int id) throws java.rmi.RemoteException {
		System.out.println("Client  : invlidate reader sur "+id);

		Stockage.get(id).invalidate_reader();
	}


	// receive a writer invalidation request from the server
	public synchronized Object invalidate_writer(int id) throws java.rmi.RemoteException {    	
		//System.out.println("Client  : invlidate writer sur "+id);

		return Stockage.get(id).invalidate_writer();
	}
}
