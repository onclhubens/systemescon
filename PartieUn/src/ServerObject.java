

import java.io.*;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

public class ServerObject implements Serializable, SharedObject_itf {

	private TypeOfLock lock;
	public Object obj;
	private int ID;
	private Client_itf writer;

	private ArrayList<Client_itf> Readers = new ArrayList<Client_itf>();

	public ServerObject(Object obj, int iD, Client in_client) {
		super();
		this.lock=TypeOfLock.RLT;
		this.obj = obj;
		this.writer=in_client;
		ID = iD;
	}

	public Client_itf getClient() {
		return writer;
	}

	public void setClient(Client_itf client) {
		this.writer = (Client)client;
	}

	public int getID() {
		return ID;
	}

	public   void unlock() {


		switch(this.lock){
		case RLT:

			this.lock=TypeOfLock.RLC;
			break;
		case RLT_WLC:
			this.lock=TypeOfLock.WLC;
			break;
		case WLT:
			this.lock=TypeOfLock.WLC;
			break;
		}
		System.out.println("Server Object  : notify() ");

		notify();
	}

	// invoked by the user program on the client node
	/* Bloquer l'objet en lecture, çad empêcher les écritures. */
	public   void lock_read(Client_itf c) {

		System.out.println("Serveur Object : demande de lecture de "+ this.ID);


		switch(this.lock){
		case RLT:
			Readers.add(c);
			this.lock=TypeOfLock.RLT;

			break;
		case NL:
			Readers.add(c);
			this.lock=TypeOfLock.RLT;

			break;
		case WLT:
			try{

				if(writer!=c){
					System.out.println("Serveur Object : demande de lecture de "+ this.ID+" : modif objet");

					this.obj=writer.reduce_lock(ID);
					writer=null;

				}

			}catch (RemoteException e) {
				// TODO: handle exception
			}
			this.lock=TypeOfLock.RLT;

			break;
		default:
			System.out.println("Server Object : lock read ne fait rien");

			break;
		}


	}

	// invoked by the user program on the client node
	public   void lock_write(Client_itf c) {
		System.out.println("Serveur Object  : demande d'ecriture "+ this.ID);


		switch(this.lock){

		case WLT:
			try{
				System.out.println("Serveur Object  : demande d'invalidation ecriture "+ this.ID);

				this.obj=this.writer.invalidate_writer(ID);
				System.out.println("Serveur Object  : demande d'invalidation ecriture "+ this.ID+" modif objet");

			}catch (RemoteException e) {
			}
			//on change l'ecrivain
			this.writer=c;
			this.lock=TypeOfLock.WLT;
			break;
		case RLT:
			// on invalide tous les lecteurs sauf celui qui veut ecrire
			System.out.println("Serveur Object  : demande d'invalidation lecture RLT "+ this.ID);
			Readers.remove(c);

			System.out.println("Serveur Object  : il y a "+ Readers.size()+" lecteurs à invalider");

			System.out.println("Serveur Object  : apres retrait de soi meme, il y a "+ Readers.size()+" lecteurs à invalider pour l'objet "+this.ID);

			if (Readers.size() != 0){
				//				for (Iterator<Client_itf> it = Readers.iterator(); it.hasNext();){
				//					try{ 
				//						Client_itf next = Readers.iterator().next();
				//						System.out.println("Serveur Object  : il y a "+ Readers.size()+" lecteurs à invalider");
				//
				//						System.out.println("Serveur Object  : next!=c = "+ (next!=c));
				//
				//						if ( next!=c)
				//						{
				//							next.invalidate_reader(ID);
				//							
				//
				//						}
				//
				//
				//					}catch(RemoteException e){
				//						System.out.println("Serveur Object  : oups");
				//
				//					}
				//				}

				for(int i=0; i<Readers.size(); i++){
					try {
						Readers.get(i).invalidate_reader(ID);
						System.out.println(i);
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.out.println("Serveur Object  : oups");

					}
				}

			}

			System.out.println("Serveur Object  : invalidation lectures "+ this.ID+" OK");

			//on vide la liste
			Readers.clear();
			// on ajoute l'�crivain
			this.writer=c;
			// on verouille *clic clac*
			this.lock=TypeOfLock.WLT;
			break;

		case NL:
			// on invalide tous les lecteurs
			Readers.remove(c);

			for(int i=0; i<Readers.size(); i++){
				try {
					Readers.get(i).invalidate_reader(ID);
					System.out.println(i);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Serveur Object  : oups");

				}
			}


			// on invalide l'ecrivain
			if (this.writer!=null){
				try{
					System.out.println("Serveur Object  : invalidation lecture "+ this.ID+" modif objet");

					this.obj=this.writer.invalidate_writer(ID);
				}catch (RemoteException e) {
				}
			}
			//on change l'ecrivain
			this.writer=c;
			this.lock=TypeOfLock.WLT;
			break;

		default:
			System.out.println("Server Object : lock write ne fait rien");

			break;

		}
	}

	@Override
	public   void lock_read() {
		// TODO Auto-generated method stub

	}

	@Override
	public   void lock_write() {
		// TODO Auto-generated method stub

	}

}
