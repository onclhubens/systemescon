
public class test_reader {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 500;
		test_reader a = new test_reader();
		Thread_read[] tab = new Thread_read[num];
		for (int i = 0; i < num; i++){
			tab[i] = a.new Thread_read(i);
		}
		for (int i = 0; i < num; i++){
			tab[i].run();

		}
		System.out.println("fini");

	}

	class Thread_read extends Thread{
		int num;
		
		public Thread_read(int i){
			this.num = i;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			// initialize the system
			Client.init();
					
			// look up the IRC object in the name server
			// if not found, create it, and register it in the name server
			SharedObject s = Client.lookup("IRC");
			if (s == null) {
				s = Client.create(new Sentence());
				Client.register("IRC", s);
			}
			//s.lock_write();
			//((Sentence)(s.obj)).write("testMouss = "+this.num);
			//s.unlock();
			// lock the object in read mode
			s.lock_read();
			
			// invoke the method
			
			System.out.println("\nLecture : "+((Sentence)(s.obj)).read());
			
			// unlock the object
			s.unlock();
			
		}
	}
}
